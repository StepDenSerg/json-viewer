import React, {useState} from 'react';

const JsonViewer = (props) => {
  const [isOpen, setIsOpen] = useState(props.isRoot);

  return (
      <div>
        <span onClick={() => setIsOpen(!isOpen)}>
        <div className="toggle-holder">
          {
            (props.value && typeof props.value === 'object' && !props.isRoot) ?
              <span className="open-btn">{isOpen ? '-' : '+'}</span> : ''
          }
        </div>
        <span className="json-label">{props.label}{props.isRoot ? '' : ':'}</span>
          </span>
        <div className="json-value">
          {getCurrentValueTemplate(props.value, isOpen)}
        </div>
      </div>
  )
};

function getCurrentValueTemplate(data, isOpen) {
  if (Array.isArray(data)) {
    return isOpen ?
        getArrayTemplate(data) :
        <span>[...] <span className="extra-info">({data.length})</span></span>
  } else if (data === null) {
    return <span className='null-undefined'>null</span>;
  } else if (typeof data === 'undefined') {
    return <span className='null-undefined'>undefined</span>;
  } else if (typeof data === 'object') {
    const keys = Object.keys(data);
    return isOpen ?
        getObjectTemplate(keys, data) :
        <span>{'{...} '} <span className="extra-info">({`${keys.slice(0, 10).join(', ')}...`})</span></span>
  } else {
    const typeOfData = typeof data;
    return <span className={'value-' + typeOfData}>{typeOfData === 'string' ? "'" : ""}{data + ''}{typeOfData === 'string' ? "'" : ""}</span>;
  }
}

function getArrayTemplate(data) {
  return (
      <React.Fragment>
        {'['}
        <br/>
        {data.map((item, index) => (
            <JsonViewer key={index} value={item} label={index}></JsonViewer>
        ))}
        {']'}
      </React.Fragment>
  )
}

function getObjectTemplate(keys, data) {
  return (
      <React.Fragment>
        {'{'}
        <br/>
        {keys.map((key, index) => (
            <JsonViewer label={key} value={data[key]} key={index}></JsonViewer>
        ))}
        {'}'}
      </React.Fragment>
  )
}

export default JsonViewer;
