import React from 'react';
import './App.css';
import JsonViewer from "./json-viewer";

const initData = {
  name: 'Name',
  age: 26,
  arr1: [
    'arr1-1',
    'arr1-2',
    'arr1-3',
    'arr1-4',
  ],
  arr2: [
    'arr2-1',
    ['arr2-2', 'arr2-3'],
    {objArr2: 'objArr2'},
    null,
    undefined
  ],
  obj1: {
    objArr: [1, 2, 3],
    obj2: {
      objArr: [4, 5, 6],
      str: 'str',
      obj21: {
        arr: [7, 8, 9]
      }
    }
  },
  n: null,
  u: undefined
};

export const App = () => (<JsonViewer value={initData} isRoot={true}></JsonViewer>);
